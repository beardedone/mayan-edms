# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Roberto Rosario, 2019
# Mohammed ALDOUB <voulnet@gmail.com>, 2019
# Marwan Rahhal <Marwanr@sssit.net>, 2020
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-28 13:01+0000\n"
"PO-Revision-Date: 2019-12-03 05:23+0000\n"
"Last-Translator: Marwan Rahhal <Marwanr@sssit.net>, 2020\n"
"Language-Team: Arabic (https://www.transifex.com/rosarior/teams/13584/ar/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ar\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: apps.py:28 events.py:6 permissions.py:5
msgid "File caching"
msgstr "الحفظ المؤقت"

#: events.py:10
msgid "Cache created"
msgstr "إنشاء حفظ مؤقت"

#: events.py:13
msgid "Cache edited"
msgstr "تعديل الحفظ المؤقت"

#: events.py:16
msgid "Cache purged"
msgstr "الغاء الحفظ المؤقت"

#: links.py:21
msgid "File caches"
msgstr "تخزين مؤقت"

#: links.py:26 links.py:30
msgid "Purge cache"
msgstr "ازالة التخزين المؤقت"

#: models.py:28
msgid "Internal name of the defined storage for this cache."
msgstr "تعريف الذاكرة المؤقتة"

#: models.py:29
msgid "Defined storage name"
msgstr "التخزين المحدد"

#: models.py:32
msgid "Maximum size of the cache in bytes."
msgstr "الحد الأقصى لملف التخزين المؤقت"

#: models.py:34 models.py:53
msgid "Maximum size"
msgstr "أكبر حجم للملف"

#: models.py:38 models.py:136
msgid "Cache"
msgstr "ملف مؤقت"

#: models.py:39
msgid "Caches"
msgstr "الملفات المؤقتة"

#: models.py:51
msgid "Size at which the cache will start deleting old entries."
msgstr "الحجم الإفتراضي لملف التخزين المؤقت"

#: models.py:60
msgid "Unknown"
msgstr "غير معرف"

#: models.py:78
msgid "Current size"
msgstr "الحجم الحالي"

#: models.py:79
msgid "Current size of the cache."
msgstr "الحجم الحالي للملف المؤقت"

#: models.py:139
msgid "Name"
msgstr "اسم"

#: models.py:144 models.py:217
msgid "Cache partition"
msgstr "تقسيم الملف المؤقت"

#: models.py:145
msgid "Cache partitions"
msgstr "تقسيم الملفات المؤقتة"

#: models.py:220
msgid "Date time"
msgstr "التاريخ والوقت"

#: models.py:222
msgid "Filename"
msgstr "اسم الملف"

#: models.py:224
msgid "File size"
msgstr "حجم الملف"

#: models.py:232
msgid "Cache partition file"
msgstr "قسم الذاكرة المؤقتة"

#: models.py:233
msgid "Cache partition files"
msgstr "قسم الملفات المؤقتة"

#: permissions.py:8 queues.py:7
msgid "Purge a file cache"
msgstr "إزالة ملفات الذاكرة"

#: permissions.py:11
msgid "View a file cache"
msgstr "إستعراض ملفات الذاكرة المؤقتة"

#: views.py:21
msgid "File caches list"
msgstr "حفظ الملفات المؤقتة"

#: views.py:37
msgid "Submit the selected cache for purging?"
msgid_plural "Submit the selected caches for purging?"
msgstr[0] "إرسال ذاكرات التخزين المؤقت المحددة للتطهير؟"
msgstr[1] "إرسال ذاكرة التخزين المؤقت المحددة للتصفية؟"
msgstr[2] "إرسال ذاكرات التخزين المؤقت المحددة للتطهير؟"
msgstr[3] "إرسال ذاكرات التخزين المؤقت المحددة للتطهير؟"
msgstr[4] "إرسال ذاكرات التخزين المؤقت المحددة للتطهير؟"
msgstr[5] "إرسال ذاكرات التخزين المؤقت المحددة للتطهير؟"
