# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Translators:
# Ayoub BENSAKHRIA <bensakhria.ayoub@gmail.com>, 2020
# Marwan Rahhal <Marwanr@sssit.net>, 2020
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-28 13:01+0000\n"
"PO-Revision-Date: 2020-12-23 23:58+0000\n"
"Last-Translator: Marwan Rahhal <Marwanr@sssit.net>\n"
"Language-Team: Arabic (http://www.transifex.com/rosarior/mayan-edms/language/ar/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ar\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"

#: apps.py:23 classes.py:142 links.py:26
msgid "ACLs"
msgstr "قوائم الصلاحيات"

#: events.py:6 permissions.py:5
msgid "Access control lists"
msgstr "المستخدمين والصلاحيات"

#: events.py:10
msgid "ACL created"
msgstr "إنشاء الصلاحيات"

#: events.py:13
msgid "ACL edited"
msgstr "تعديل الصلاحيات"

#: forms.py:13 models.py:44 views.py:67
msgid "Role"
msgstr "الدور"

#: links.py:15
msgid "New ACL"
msgstr "إنشاء صلاحيات جديدة"

#: links.py:20
msgid "Delete"
msgstr "حذف"

#: links.py:31 models.py:40 workflow_actions.py:46 workflow_actions.py:161
msgid "Permissions"
msgstr "الصلاحيات"

#: managers.py:229
#, python-format
msgid "Object \"%s\" is not a model and cannot be checked for access."
msgstr "الكائن \"%s\" ليس نموذجًا ولا يمكن التحقق منه للولوج. "

#: managers.py:250
#, python-format
msgid "Insufficient access for: %s"
msgstr "صلاحيات الوصول غير كافية لـ:%s"

#: models.py:52
msgid "Access entry"
msgstr "بوابة الدخول"

#: models.py:53
msgid "Access entries"
msgstr "بوابات الدخول"

#: models.py:57
#, python-format
msgid "Role \"%(role)s\" permission's for \"%(object)s\""
msgstr "إذن الدور \"%(role)s\" لـ \"%(object)s\""

#: permissions.py:8
msgid "Edit ACLs"
msgstr "تعديل قوائم الصلاحيات"

#: permissions.py:11
msgid "View ACLs"
msgstr "إستعراض قوائم الصلاحيات"

#: serializers.py:24 serializers.py:134
msgid ""
"API URL pointing to the list of permissions for this access control list."
msgstr "عنوان URL لواجهة برمجة التطبيقات يشير إلى قائمة الصلاحيات لقائمة التحكم في الوصول هذه."

#: serializers.py:57
msgid ""
"API URL pointing to a permission in relation to the access control list to "
"which it is attached. This URL is different than the canonical workflow URL."
msgstr "يشير عنوان URL الخاص بواجهة برمجة التطبيقات إلى صلاحية تتعلق بقائمة التحكم في الدخول المرفقة به. يختلف عنوان URL هذا عن عنوان URL الأساسي لسير العمل."

#: serializers.py:89
msgid "Primary key of the new permission to grant to the access control list."
msgstr "المفتاح الأساسي للصلاحية الجديدة لإتاحة الولوج إلى قائمة التحكم في الدخول."

#: serializers.py:113 serializers.py:189
#, python-format
msgid "No such permission: %s"
msgstr "هذه الصلاحية غير موجودة: %s"

#: serializers.py:128
msgid ""
"Comma separated list of permission primary keys to grant to this access "
"control list."
msgstr "قائمة مفصولة بفواصل من مفاتيح الصلاحيات الأساسية لإتاحة الولوج إلى قائمة التحكم هذه."

#: serializers.py:140
msgid "Primary keys of the role to which this access control list binds to."
msgstr "المفاتيح الأساسية للدور الذي ترتبط به قائمة التحكم في الدخول هذه."

#: views.py:40
#, python-format
msgid ""
"An ACL for \"%(object)s\" using role \"%(role)s\" already exists. Edit that "
"ACL entry instead."
msgstr "توجد بالفعل قائمة صلاحيات ل \"%(object)s\" باستخدام الدور \"%(role)s\". قم بتحرير هذا العنصر من قائمة صلاحيات بدلاً من ذلك."

#: views.py:53
#, python-format
msgid "New access control lists for: %s"
msgstr "إضافات للمستخدمين والصلاحيات: %s"

#: views.py:95
#, python-format
msgid "Delete ACL: %s"
msgstr "حذف قائمة التحكم في الوصول: %s"

#: views.py:133
msgid "There are no ACLs for this object"
msgstr "لا توجد قوائم تحكم في الوصول لهذا الكائن"

#: views.py:136
msgid ""
"ACL stands for Access Control List and is a precise method to control user "
"access to objects in the system. ACLs allow granting a permission to a role "
"but only for a specific object or set of objects."
msgstr "قوائم الصلاحيات تسمح للمستخدم من الدخول الى الأذونات وحسب الادوار المخصصة"

#: views.py:143
#, python-format
msgid "Access control lists for: %s"
msgstr " المستخدمين والصلاحيات: %s"

#: views.py:157
msgid "Granted permissions"
msgstr "الصلاحيات الممنوحة"

#: views.py:158
msgid "Available permissions"
msgstr "الصلاحيات المتاحة"

#: views.py:202
#, python-format
msgid "Role \"%(role)s\" permission's for \"%(object)s\"."
msgstr "صلاحية الدور \"%(role)s\" لـ \"%(object)s\"."

#: views.py:211
msgid ""
"Disabled permissions are inherited from a parent object or directly granted "
"to the role and can't be removed from this view. Inherited permissions need "
"to be removed from the parent object's ACL or from them role via the Setup "
"menu."
msgstr "يتم اكتساب الصلاحيات المعطلة من كائن رئيسي أو يتم منحها مباشرة للدور ولا يمكن إزالتها من هذا العرض. يجب إزالة الصلاحيات الموروثة من قائمة التحكم في الوصول للكائن الأصلي أو من دورها عبر قائمة الإعداد."

#: workflow_actions.py:23
msgid "Object type"
msgstr "نوع الكائن"

#: workflow_actions.py:26
msgid "Type of the object for which the access will be modified."
msgstr "نوع الكائن الذي سيتم تعديل الوصول إليه."

#: workflow_actions.py:32
msgid "Object ID"
msgstr "معرف الكائن"

#: workflow_actions.py:35
msgid ""
"Numeric identifier of the object for which the access will be modified."
msgstr "المعرف الرقمي للكائن الذي سيتم تعديل الوصول إليه."

#: workflow_actions.py:40 workflow_actions.py:155
msgid "Roles"
msgstr "الأدوار"

#: workflow_actions.py:42 workflow_actions.py:157
msgid "Roles whose access will be modified."
msgstr "الأدوار التي سيتم تعديل الوصول إليها."

#: workflow_actions.py:49 workflow_actions.py:164
msgid ""
"Permissions to grant/revoke to/from the role for the object selected above."
msgstr "صلاحيات منح / إبطال إلى / من دور الكائن المحدد أعلاه."

#: workflow_actions.py:57
msgid "Grant object access"
msgstr "إتاحة الوصول إلى الكائن"

#: workflow_actions.py:140
msgid "Revoke object access"
msgstr "إبطال الوصول إلى الكائن"

#: workflow_actions.py:172
msgid "Grant document access"
msgstr "منح حق الوصول إلى الوثيقة"

#: workflow_actions.py:211
msgid "Revoke document access"
msgstr "إبطال صلاحية الوصول إلى الوثيقة"
