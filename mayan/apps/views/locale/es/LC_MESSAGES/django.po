# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Lory977 <helga.carrero@gmail.com>, 2020
# Roberto Rosario, 2021
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-03-28 13:02+0000\n"
"PO-Revision-Date: 2020-09-27 06:47+0000\n"
"Last-Translator: Roberto Rosario, 2021\n"
"Language-Team: Spanish (https://www.transifex.com/rosarior/teams/13584/es/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: es\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: apps.py:15 settings.py:7
msgid "Views"
msgstr "Vistas"

#: forms.py:24
msgid "Selection"
msgstr "Selección"

#: forms.py:100
msgid "None"
msgstr "Ninguno"

#: generics.py:150
msgid ""
"Select entries to be removed. Hold Control to select multiple entries. Once "
"the selection is complete, click the button below or double click the list "
"to activate the action."
msgstr ""
"Seleccione las entradas que desea eliminar. Mantén pulsado Control para "
"seleccionar varias entradas. Una vez que se complete la selección, haga clic"
" en el botón de abajo o haga doble clic en la lista para activar la acción."

#: generics.py:155
msgid ""
"Select entries to be added. Hold Control to select multiple entries. Once "
"the selection is complete, click the button below or double click the list "
"to activate the action."
msgstr ""
"Seleccione las entradas que se añadirán. Mantén pulsado Control para "
"seleccionar varias entradas. Una vez que se complete la selección, haga clic"
" en el botón de abajo o haga doble clic en la lista para activar la acción."

#: generics.py:288
msgid "Add all"
msgstr "Añadir todos"

#: generics.py:297
msgid "Add"
msgstr "Agregar"

#: generics.py:307
msgid "Remove all"
msgstr "Remover todos"

#: generics.py:316
msgid "Remove"
msgstr "Remover"

#: generics.py:532
#, python-format
msgid "Duplicate data error: %(error)s"
msgstr "Error de datos duplicados: %(error)s"

#: generics.py:553
#, python-format
msgid "%(object)s not created, error: %(error)s"
msgstr "%(object)s no se pudo crear, error: %(error)s"

#: generics.py:566
#, python-format
msgid "%(object)s created successfully."
msgstr "%(object)s creado exitosamente."

#: generics.py:605
#, python-format
msgid "%(object)s not deleted, error: %(error)s."
msgstr "%(object)s no se pudo eliminar, error: %(error)s."

#: generics.py:614
#, python-format
msgid "%(object)s deleted successfully."
msgstr "%(object)s eliminado con éxito."

#: generics.py:750
#, python-format
msgid "%(object)s not updated, error: %(error)s."
msgstr "%(object)s no se pudo actualizar, error: %(error)s."

#: generics.py:761
#, python-format
msgid "%(object)s updated successfully."
msgstr "%(object)s actualizado con éxito."

#: mixins.py:324
#, python-format
msgid "No %(verbose_name)s found matching the query"
msgstr "No se encontró %(verbose_name)s que coincida con la consulta"

#: mixins.py:349
#, python-format
msgid "Unable to perform operation on object %(instance)s; %(exception)s."
msgstr ""
"No se puede realizar la operación en el objeto %(instance)s; %(exception)s."

#: mixins.py:351
#, python-format
msgid "Operation performed on %(object)s."
msgstr "Operación realizada en %(object)s."

#: mixins.py:352
#, python-format
msgid "Operation performed on %(count)d object."
msgstr "Operación realizada en %(count)d objeto."

#: mixins.py:353
#, python-format
msgid "Operation performed on %(count)d objects."
msgstr "Operación realizada en %(count)d objetos."

#: mixins.py:354
#, python-format
msgid "Perform operation on %(object)s."
msgstr "Realizar operación en %(object)s."

#: mixins.py:355
#, python-format
msgid "Perform operation on %(count)d object."
msgstr "Realizar operación en %(count)d objeto."

#: mixins.py:356
#, python-format
msgid "Perform operation on %(count)d objects."
msgstr "Realizar la operación en %(count)d objetos."

#: mixins.py:438
msgid "Object"
msgstr "Objeto"

#: settings.py:12
msgid "The number objects that will be displayed per page."
msgstr "El número de objetos que se mostrarán por página."

#: templatetags/views_tags.py:28
msgid "Confirm delete"
msgstr "Confirmar eliminación"

#: templatetags/views_tags.py:33
#, python-format
msgid "Edit %s"
msgstr "Editar %s"

#: templatetags/views_tags.py:36
msgid "Confirm"
msgstr "Confirmar"

#: templatetags/views_tags.py:40
#, python-format
msgid "Details for: %s"
msgstr "Detalles para: %s"

#: templatetags/views_tags.py:44
#, python-format
msgid "Edit: %s"
msgstr "Editar: %s"

#: templatetags/views_tags.py:48
msgid "Create"
msgstr "Crear"
